ARG ALERTMANAGER_BOT_VERSION=0.4.3
FROM metalmatze/alertmanager-bot:${ALERTMANAGER_BOT_VERSION}

RUN apk add --no-cache bash=~5

COPY rootfs /

ENTRYPOINT ["/usr/bin/entrypoint.sh"]