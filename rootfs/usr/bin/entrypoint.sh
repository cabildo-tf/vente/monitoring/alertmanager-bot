#!/bin/bash -e

ARGS=""
for admin in $(echo "${TELEGRAM_ADMINS}" | tr ',' ' ')
do
	ARGS+="--telegram.admin=${admin} "
done


# Force all args into alertmanager
if [[ "$1" = '/usr/bin/alertmanager-bot' ]]; then
  shift
fi


exec /usr/bin/alertmanager-bot ${ARGS} $@